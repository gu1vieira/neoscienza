﻿using Neoscienza.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Neoscienza.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginView : ContentPage
    {
        private readonly LoginViewModel _loginViewModel;

        public LoginView()
        {
            _loginViewModel = new LoginViewModel();
            InitializeComponent();
            BindingContext = _loginViewModel;
            this.IsBusy = false;
        }

        async void CadastroButton(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new CadastroView());
        }

        //define a propriedade IsBusy como true
        private async void BtnLogin_Clicked(object sender, EventArgs e)
        {
            //ativa o ActivityIndicator
            this.IsBusy = true;
            // aqui ficaria o seu código 
            // para fazer a autenticação
            await DandoUmTempo(3000);
            Application.Current.MainPage = new NavigationPage(new MainPage());
        }
        //Dá uma pausa de 5 segundos
        async Task DandoUmTempo(int valor)
        {
            await Task.Delay(valor);
        }
    }
}