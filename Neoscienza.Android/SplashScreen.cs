﻿using Android.App;
using Android.OS;
using System.Timers;

namespace Neoscienza.Droid
{
    [Activity(Icon = "@drawable/homer", Theme = "@style/Theme.Splash", MainLauncher = true, NoHistory = true)]
    public class SplashScreen : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Timer timer = new Timer();
            timer.Interval = 1000; // 3 sec.
            timer.AutoReset = false; // Do not reset the timer after it's elapsed
            timer.Elapsed += (object sender, ElapsedEventArgs e) =>
            {
                StartActivity(typeof(SplashActivity2));
            };
            timer.Start();
        }
    }

    [Activity(Theme = "@style/Theme2.Splash", NoHistory = true)]
    public class SplashActivity2 : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            Timer timer = new Timer();
            timer.Interval = 1000; // 3 sec.
            timer.AutoReset = false; // Do not reset the timer after it's elapsed
            timer.Elapsed += (object sender, ElapsedEventArgs e) =>
            {
                StartActivity(typeof(MainActivity));
            };
            timer.Start();
        }
    };

}