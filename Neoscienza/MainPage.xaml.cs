﻿using Neoscienza.Views;
using System;
using Xamarin.Forms;

//https://github.com/xamarin/xamarin-forms-samples/tree/master/Navigation/LoginFlow/LoginNavigation

namespace Neoscienza
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        async void OnLogoutButtonClicked(object sender, EventArgs e)
        {
            App.IsUserLoggedIn = false;
            Navigation.InsertPageBefore(new LoginView(), this);
            await Navigation.PopAsync();
        }
    }
}
