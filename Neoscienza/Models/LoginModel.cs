﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Neoscienza.Models
{
    class LoginModel : BaseModel
    {
        private string _email;
        private string _senha;
        public string Email { get => _email; set { _email = value; OnPropertyChanged(); } }
        public string Senha { get => _senha; set { _senha = value; OnPropertyChanged(); } }
    }
}
