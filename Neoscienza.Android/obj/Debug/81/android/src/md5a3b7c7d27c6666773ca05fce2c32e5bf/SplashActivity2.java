package md5a3b7c7d27c6666773ca05fce2c32e5bf;


public class SplashActivity2
	extends android.app.Activity
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onCreate:(Landroid/os/Bundle;)V:GetOnCreate_Landroid_os_Bundle_Handler\n" +
			"";
		mono.android.Runtime.register ("Neoscienza.Droid.SplashActivity2, Neoscienza.Android", SplashActivity2.class, __md_methods);
	}


	public SplashActivity2 ()
	{
		super ();
		if (getClass () == SplashActivity2.class)
			mono.android.TypeManager.Activate ("Neoscienza.Droid.SplashActivity2, Neoscienza.Android", "", this, new java.lang.Object[] {  });
	}


	public void onCreate (android.os.Bundle p0)
	{
		n_onCreate (p0);
	}

	private native void n_onCreate (android.os.Bundle p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
